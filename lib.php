<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Callbacks used by Moodle API.
 * @package    local_dyslexia
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Callback to add head elements.
 *
 * @return string A valid html head fragment
 */
function local_dyslexia_before_standard_html_head() {
    if (get_user_preferences('local_dyslexia_enable', 0) == 1) {
        $font = get_user_preferences('local_dyslexia_font', local_dyslexia\form::get_setting_default('font'));
        $hspacing = get_user_preferences('local_dyslexia_hspacing', local_dyslexia\form::get_setting_default('hspacing'));
        $vspacing = get_user_preferences('local_dyslexia_vspacing', local_dyslexia\form::get_setting_default('vspacing'));
        $rules = [];
        if ($hspacing) {
            $rules[] = '--local_dyslexia_hspacing:' . $hspacing;
        }
        if ($vspacing) {
            $rules[] = '--local_dyslexia_vspacing:' . $vspacing;
        }
        $scss = !empty($rules) ? html_writer::tag('style', ':root{' . implode(';', $rules) . '}') : '';
        return $scss . html_writer::empty_tag('link',
                [
                        'rel' => 'stylesheet',
                        'type' => 'text/css',
                        'href' =>  new moodle_url('/local/dyslexia/css/dyslexia_' . $font . '.css'),
                ]);
    } else {
        return '';
    }
}

/**
 * Callback to extend the navigation for user settings node.
 *
 * @param navigation_node $navigation The navigation node to extend
 * @param stdClass $user The user object
 * @param context $usercontext The context of the user
 * @param stdClass $course The course object
 * @param context $coursecontext The context of the course
 */
function local_dyslexia_extend_navigation_user_settings($navigation, $user, $usercontext, $course, $coursecontext) {
    $url = new moodle_url('/local/dyslexia/edit.php', [ 'id' => $user->id, 'course' => $course->id ]);
    $subsnode = navigation_node::create(get_string('dyslexiamode', 'local_dyslexia'), $url, navigation_node::TYPE_SETTING);
    $navigation->add_node($subsnode);
}
