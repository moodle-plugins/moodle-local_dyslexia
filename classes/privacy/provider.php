<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Privacy subsystem implementation for local_dyslexia.
 * @package    local_dyslexia
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_dyslexia\privacy;

use context_system;
use core_privacy\local\metadata\collection;
use core_privacy\local\request\transform;
use core_privacy\local\request\writer;

/**
 * Privacy provider class for local_dyslexia.
 *
 * The only personal data managed by this plugin is the "local_dyslexia_enable" user preference.
 *
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class provider implements \core_privacy\local\metadata\provider,
                          \core_privacy\local\request\user_preference_provider {

    /**
     * Return the fields and user preferences which are considered personal data.
     *
     * @param collection $collection a reference to the collection to use to store the metadata.
     * @return collection the updated collection of metadata items.
     */
    public static function get_metadata(collection $collection): collection {
        $collection->add_user_preference('local_dyslexia_enable', 'privacy:metadata:local_dyslexia_enable');
        $collection->add_user_preference('local_dyslexia_font', 'privacy:metadata:local_dyslexia_font');
        return $collection;
    }

    /**
     * Export user preferences.
     *
     * @param int $userid The userid of the user to export preferences
     */
    public static function export_user_preferences(int $userid) {
        $writer = writer::with_context(context_system::instance());

        $value = get_user_preferences('local_dyslexia_enable', null, $userid);
        if ($value !== null) {
            $value = transform::yesno($value);
            $description = get_string('privacy:metadata:local_dyslexia_enable', 'local_dyslexia');
            $writer->export_user_preference('local_dyslexia', 'local_dyslexia_enable', $value, $description);
        }

        $value = get_user_preferences('local_dyslexia_font', null, $userid);
        if ($value !== null) {
            $value = \local_dyslexia\form::get_select_options('font')[$value];
            $description = get_string('privacy:metadata:local_dyslexia_font', 'local_dyslexia');
            $writer->export_user_preference('local_dyslexia', 'local_dyslexia_font', $value, $description);
        }

        $value = get_user_preferences('local_dyslexia_hspacing', null, $userid);
        if ($value !== null) {
            $value = \local_dyslexia\form::get_select_options('hspacing')[$value];
            $description = get_string('privacy:metadata:local_dyslexia_hspacing', 'local_dyslexia');
            $writer->export_user_preference('local_dyslexia', 'local_dyslexia_hspacing', $value, $description);
        }

        $value = get_user_preferences('local_dyslexia_vspacing', null, $userid);
        if ($value !== null) {
            $value = \local_dyslexia\form::get_select_options('vspacing')[$value];
            $description = get_string('privacy:metadata:local_dyslexia_vspacing', 'local_dyslexia');
            $writer->export_user_preference('local_dyslexia', 'local_dyslexia_vspacing', $value, $description);
        }
    }

}
