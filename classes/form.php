<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * A simple enable/disable form for dyslexia mode.
 * @package    local_dyslexia
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

namespace local_dyslexia;

use html_writer;
use moodleform;

defined('MOODLE_INTERNAL') || die();

global $CFG;
require_once($CFG->libdir . '/formslib.php');

/**
 * Dyslexia mode enable/disable form.
 *
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class form extends moodleform {

    /**
     * {@inheritDoc}
     * @see moodleform::definition()
     */
    protected function definition() {
        $mform = $this->_form;

        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        $mform->addElement('hidden', 'course');
        $mform->setType('course', PARAM_INT);

        $mform->addElement('selectyesno', 'enable', get_string('enabledyslexiamode', 'local_dyslexia'));
        $mform->setDefault('enable', 0);
        $description = html_writer::span(get_string('enabledyslexiamode_desc', 'local_dyslexia'));
        $mform->addElement('static', null, null, $description);

        $mform->addElement('select', 'font', get_string('font', 'local_dyslexia'), static::get_select_options('font'));
        $mform->setDefault('font', static::get_setting_default('font'));
        $mform->disabledIf('font', 'enable', 'neq', 1);

        $mform->addElement('select', 'hspacing', get_string('hspacing', 'local_dyslexia'), static::get_select_options('hspacing'));
        $mform->setDefault('hspacing', static::get_setting_default('hspacing'));
        $mform->disabledIf('hspacing', 'enable', 'neq', 1);

        $mform->addElement('select', 'vspacing', get_string('vspacing', 'local_dyslexia'), static::get_select_options('vspacing'));
        $mform->setDefault('vspacing', static::get_setting_default('vspacing'));
        $mform->disabledIf('vspacing', 'enable', 'neq', 1);

        $this->add_action_buttons(true, get_string('savechanges'));
    }

    public static function get_select_options($setting) {
        return [
                'font' => [
                        'OD' => 'OpenDyslexic',
                        'CS' => 'Comic Sans',
                ],
                'hspacing' => [
                        '' => get_string('normal'),
                        '.1ch' => '+ 10%',
                        '.2ch' => '+ 20%',
                        '.3ch' => '+ 30%',
                        '.4ch' => '+ 40%',
                        '.5ch' => '+ 50%',
                ],
                'vspacing' => [
                        '' => get_string('normal'),
                        '1.7' => '+ 20%',
                        '1.9' => '+ 40%',
                        '2.1' => '+ 60%',
                        '2.3' => '+ 80%',
                        '2.5' => '+ 100%',
                ],
        ][$setting];
    }

    public static function get_setting_default($setting) {
        return [
                'font' => 'OD',
                'hspacing' => '.1ch',
                'vspacing' => '1.7',
        ][$setting];
    }
}
