<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page to change user preference for dyslexia mode settings.
 * @package    local_dyslexia
 * @copyright  2024 Astor Bizard
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__ . '/../../config.php');

global $CFG, $USER, $PAGE, $OUTPUT;

require_login();

require_once($CFG->dirroot . '/user/editlib.php');

$userid = optional_param('id', $USER->id, PARAM_INT);    // User id.
$courseid = optional_param('course', SITEID, PARAM_INT);   // Course id (defaults to Site).

$PAGE->set_url('/local/dyslexia/edit.php', [ 'id' => $userid, 'course' => $courseid ]);

// Use this to properly look like a user preference page.
// It also does all the necessary access checks.
list($user, $course) = useredit_setup_preference_page($userid, $courseid);

$form = new local_dyslexia\form();
$form->set_data([
        'id' => $userid,
        'course' => $course->id,
        'enable' => get_user_preferences('local_dyslexia_enable', 0, $user),
        'font' => get_user_preferences('local_dyslexia_font', local_dyslexia\form::get_setting_default('font'), $user),
        'hspacing' => get_user_preferences('local_dyslexia_hspacing', local_dyslexia\form::get_setting_default('hspacing'), $user),
        'vspacing' => get_user_preferences('local_dyslexia_vspacing', local_dyslexia\form::get_setting_default('vspacing'), $user),
]);

if ($form->is_cancelled()) {
    redirect(new moodle_url('/user/preferences.php', [ 'userid' => $user->id ]));
} else if (($data = $form->get_data()) !== null) {
    require_sesskey();
    // Simply update user preference and redirect.
    set_user_preference('local_dyslexia_enable', $data->enable);
    if (isset($data->font)) {
        set_user_preference('local_dyslexia_font', $data->font);
    }
    if (isset($data->hspacing)) {
        set_user_preference('local_dyslexia_hspacing', $data->hspacing);
    }
    if (isset($data->vspacing)) {
        set_user_preference('local_dyslexia_vspacing', $data->vspacing);
    }
    redirect($PAGE->url);
}

$title = get_string('dyslexiamode', 'local_dyslexia');
$userfullname = fullname($user);

$PAGE->set_title(get_string('userpreferences') . ' - ' . $title);
$PAGE->set_heading($userfullname);

echo $OUTPUT->header();
echo $OUTPUT->heading($title);

$form->display();

echo $OUTPUT->footer();
