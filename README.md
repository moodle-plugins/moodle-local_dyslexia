## Dyslexia mode for Moodle

This plugin adds a user preference to use Dyslexia mode.  

Dyslexia mode is meant to improve readability and accessibility for dyslexic users.  
It will change the font throughout the site to [OpenDyslexic](https://opendyslexic.org/), or to another font (currently supported fonts: OpenDyslexic, Comic Sans). It will also increase letter and word spacing.  

Each user can enable or disable it on their preferences page.  

### About

This software was developped with the Caseine project, with the support of Université Grenoble Alpes.  
